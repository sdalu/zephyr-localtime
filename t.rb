
$define = [ 'AMERICA',
            'ASIA',
            'AUSTRALIA',
            'ETC',
            'EUROPE',
            'AFRICA',
            'INDIAN',
            'PACIFIC',
            'ATLANTIC',
            'ANTARCTICA',
            'ARCTIC',
          ]

(1..($define.size)).flat_map {|i|
    $define.combination(i).to_a
}.each {|d|
    system('cc', 't.c', *d.map {|_| "-DLOCALTIME_TZ_#{_}=1" })
#    puts abbrv.inspect


    puts "#if " + (d.map {|k| "LOCALTIME_TZ_#{k}==1" } +
     ($define - d).map {|k| "LOCALTIME_TZ_#{k}==0" }).join(' && ')
    puts `./a.out`.split.uniq.reject {|k| k.empty? }.sort.map {|a| "#{a}\\0" }.join
    puts "#endif"
}
