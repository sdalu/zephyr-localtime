#ifndef __LOCALTIME_H
#define __LOCALTIME_H

#define LOCALTIME_TZ_MAXLEN 32
#define LOCALTIME_LANG_MAXLEN 5

extern const char *_localtime_posix;

#define LOCALTIME_POSIX _localtime_posix

/* Forward declaration */
struct localtime_tz;
struct localtime_lang;

/* Type definition */
typedef struct localtime_tz   *localtime_tz_t;
typedef struct localtime_lang *localtime_lang_t;

/* Timezone and lang definition */
const localtime_tz_t localtime_timezone(const char *timezone, const char *fallback);
const localtime_lang_t localtime_lang(const char *lang, const char *fallback);

struct tm *gmtime_r(const time_t *timer, struct tm *tm);

int localtime_tz(const struct timespec *timespec, struct tm *tm,
		 const localtime_tz_t tz);
int localtime_utc(const struct timespec *timespec, struct tm *tm);

int mktime_tz(const struct tm *tm, struct timespec *result,
	      const localtime_tz_t tz);
int mktime_utc(const struct tm *tm, struct timespec *result);

size_t strftime_lang(char *s, size_t maxsize, const char *format,
		     const struct tm *tm, const localtime_lang_t lang);

#endif
