// Copyright (c) 2015-2016 Nuxi, https://nuxi.nl/
//
// SPDX-License-Identifier: BSD-2-Clause

#ifndef COMMON_LOCALE_H
#define COMMON_LOCALE_H

#include <sys/types.h>

#include <assert.h>
#include <errno.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

// LC_TIME.

struct lc_time {
  const char *d_t_fmt;      // String for formatting date and time.
  const char *d_fmt;        // Date format string.
  const char *t_fmt;        // Time format string.
  const char *t_fmt_ampm;   // a.m. or p.m. time format string.
  const char *am_str;       // Ante-meridiem affix.
  const char *pm_str;       // Post-meridiem affix.
  const char *day[7];       // Names of the days of the week.
  const char *abday[7];     // Abbreviated names of the days of the week.
  const char *mon[12];      // Names of the months of the year.
  const char *abmon[12];    // Abbreviated names of the months of the year.
  const char *era;          // Optional: Era description segments.
  const char *era_d_fmt;    // Optional: Era date format string.
  const char *era_d_t_fmt;  // Optional: Era date and time format string.
  const char *era_t_fmt;    // Optional: Era time format string.
  const char *alt_digits;   // Optional: Alternative symbols for digits.
};

extern const struct lc_time __time_posix;

// LC_TIMEZONE.

struct lc_timezone_rule {
  uint8_t year_from;      // Lowest year to which this rule applies.
  uint8_t year_to;        // Highest year to which this rule applies.
  uint32_t month : 4;     // Month at which this rule starts.
  uint32_t weekday : 3;   // Weekday at which this rule start (7=dontcare).
  uint32_t monthday : 5;  // Day of the month at which this rule starts.
  uint32_t minute : 11;   // Minutes since 0:00 at which this rule starts.
  uint32_t timebase : 2;  // Which time should be compared against.
#define TIMEBASE_CUR 0    // Rule applies to the current offset.
#define TIMEBASE_STD 1    // Rule applies to the standard time.
#define TIMEBASE_UTC 2    // Rule applies to time in UTC.
  uint32_t save : 4;      // The amount of time in 10 minutes.
  char abbreviation[6];   // Abbreviation of timezone name (e.g., CEST).
};

struct lc_timezone_era {
  const struct lc_timezone_rule *rules;  // Rules associated with this era.
  uint64_t rules_count : 8;              // Number of rules.
  int64_t gmtoff : 18;                   // Offset in seconds relative to GMT.
  int64_t end : 38;                      // Timestamp at which this era ends.
  uint8_t end_save : 4;                  // Daylight savings at the end time.
  char abbreviation_std[6];  // Abbreviation of standard time (e.g., CET).
  char abbreviation_dst[6];  // Abbreviation of DST (e.g., CEST).
};

struct lc_timezone {
  const struct lc_timezone_era *eras;
  size_t eras_count;
};

extern const struct lc_timezone __timezone_utc;

#endif
