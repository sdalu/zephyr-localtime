// Copyright (c) 2015 Nuxi, https://nuxi.nl/
//
// SPDX-License-Identifier: BSD-2-Clause

#include <common/locale.h>

//static
const struct lc_time time_fr_fr = {
    .d_t_fmt = "%a %e %b %H:%M:%S %Y",
    .d_fmt = "%d.%m.%Y",
    .t_fmt = "%H:%M:%S",
    .t_fmt_ampm = "",
    .am_str = "",
    .pm_str = "",
    .day = {"dimanche", "lundi", "mardi", "mercredi", "jeudi", "vendredi",
            "samedi"},
    .abday = {"dim", "lun", "mar", "mer", "jeu", "ven", "sam"},
    .mon = {"janvier", "février", "mars", "avril", "mai", "juin",
            "juillet", "août", "septembre", "octobre", "novembre",
            "décembre"},
    .abmon = {"jan", "fév", "mar", "avr", "mai", "jui", "jul", "aoû",
              "sep", "oct", "nov", "déc"},
};
