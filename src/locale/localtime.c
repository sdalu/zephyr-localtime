// Copyright (c) 2015-2016 Nuxi, https://nuxi.nl/
//
// SPDX-License-Identifier: BSD-2-Clause

#include <common/locale.h>

#include <errno.h>
#include <stdlib.h>
#include <string.h>

#include "locale/timezone/tzdata.h"

#include "time/en_us.h"
#include "time/fr_fr.h"

#ifndef __arraycount
#define __arraycount(x) (sizeof(x) / sizeof((x)[0]))
#endif

const char *_localtime_posix = "POSIX";

static struct {
  char name[6];
  const struct lc_time *time;
} languages[] = {
#if LOCALTIME_LANG_EN
    {"en",    &time_en_us},
#endif
#if LOCALTIME_LANG_FR
    {"fr",    &time_fr_fr},
#endif
};


const struct lc_time *__localtime_lang(const char *lang)
{
  if (*lang == '\0' || strcmp(lang, "C") == 0 ||
      strcmp(lang, "POSIX") == 0) {
    return &__time_posix;
  }
  for (size_t i = 0; i < __arraycount(languages); ++i) {
    if (strcmp(lang, languages[i].name) == 0) {
	return languages[i].time;
    }
  }
  if ((strlen(lang) > 3) && (lang[2] == '_')) {
    char short_lang[] = { lang[0], lang[1], 0 };
    for (size_t i = 0; i < __arraycount(languages); ++i) {
      if (strcmp(short_lang, languages[i].name) == 0) {
	return languages[i].time;
      }
    }
  }
  errno = ENOENT;
  return NULL;
}

const struct lc_time *localtime_lang(const char *lang, const char *fallback)
{
   const struct lc_time *res = __localtime_lang(lang);
   if (res == NULL) {
       if (fallback != NULL) {
	   res = __localtime_lang(fallback);
       }
   }
   return res;
}

const struct lc_timezone *__localtime_timezone(const char *timezone) {
  if (*timezone == '\0' || strcmp(timezone, "C") == 0 ||
      strcmp(timezone, "POSIX") == 0) {
    return &__timezone_utc;
  }
  size_t i = 0;
  const char *timezone_name = timezone_names;
  do {
    if (strcmp(timezone, timezone_name) == 0) {
      return &timezones[i];
    }
    ++i;
    timezone_name += strlen(timezone_name) + 1;
  } while (*timezone_name != '\0');
  errno = ENOENT;
  return NULL;
}  

const struct lc_timezone *localtime_timezone(const char *timezone, const char *fallback) {
   const struct lc_timezone *res = __localtime_timezone(timezone);
   if (res == NULL) {
       if (fallback != NULL) {
	   res = __localtime_timezone(fallback);
       }
   }
   return res;
}
