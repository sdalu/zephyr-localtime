Port of the Cloudlibc[1] localtime/mktimestrftime to zephyr[2].

The following functions are available:
* localtime_utc / localtime_tz
* mktime_utc / mktime_tz
* strftime_lang

Getting timezone and lang definition using:
* localtime_timezone / localtime_lang

~~~c
const localtime_tz_t localtime_timezone(const char *timezone, const char *fallback);
const localtime_lang_t localtime_lang(const char *lang, const char *fallback);

int localtime_tz(const struct timespec *timespec, struct tm *tm, const localtime_tz_t tz);
int localtime_utc(const struct timespec *timespec, struct tm *tm);

int mktime_tz(const struct tm *tm, struct timespec *result, const localtime_tz_t tz);
int mktime_utc(const struct tm *tm, struct timespec *result);

size_t strftime_lang(char *s, size_t maxsize, const char *format,
                     const struct tm *tm, const localtime_lang_t lang);
~~~

[1] https://github.com/NuxiNL/cloudlibc
[2] https://www.zephyrproject.org/
