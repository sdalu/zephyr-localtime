#include <unistd.h>
#include <stdio.h>
#include <string.h>


#define LOCALTIME_TZ_AMERICA		1
#define LOCALTIME_TZ_ASIA		1
#define LOCALTIME_TZ_AUSTRALIA		1
#define LOCALTIME_TZ_ETC		1
#define LOCALTIME_TZ_EUROPE		1
#define LOCALTIME_TZ_AFRICA		1
#define LOCALTIME_TZ_INDIAN		1
#define LOCALTIME_TZ_PACIFIC		1
#define LOCALTIME_TZ_ATLANTIC		1
#define LOCALTIME_TZ_ANTARCTICA		1
#define LOCALTIME_TZ_ARCTIC		1


struct lc_timezone {
  const struct lc_timezone_era *eras;
  size_t eras_count;
};

struct lc_timezone_rule {
  uint8_t year_from;      // Lowest year to which this rule applies.
  uint8_t year_to;        // Highest year to which this rule applies.
  uint32_t month : 4;     // Month at which this rule starts.
  uint32_t weekday : 3;   // Weekday at which this rule start (7=dontcare).
  uint32_t monthday : 5;  // Day of the month at which this rule starts.
  uint32_t minute : 11;   // Minutes since 0:00 at which this rule starts.
  uint32_t timebase : 2;  // Which time should be compared against.
#define TIMEBASE_CUR 0    // Rule applies to the current offset.
#define TIMEBASE_STD 1    // Rule applies to the standard time.
#define TIMEBASE_UTC 2    // Rule applies to time in UTC.
  uint32_t save : 4;      // The amount of time in 10 minutes.
  char abbreviation[6];   // Abbreviation of timezone name (e.g., CEST).
};

struct lc_timezone_era {
  const struct lc_timezone_rule *rules;  // Rules associated with this era.
  uint64_t rules_count : 8;              // Number of rules.
  int64_t gmtoff : 18;                   // Offset in seconds relative to GMT.
  int64_t end : 38;                      // Timestamp at which this era ends.
  uint8_t end_save : 4;                  // Daylight savings at the end time.
  char abbreviation_std[6];  // Abbreviation of standard time (e.g., CET).
  char abbreviation_dst[6];  // Abbreviation of DST (e.g., CEST).
};


#include "src/locale/timezone/tzdata.h"

#define __arraycount(x) (sizeof(x) / sizeof((x)[0]))

int main(void)
{

    for (int i = 0 ; i < __arraycount(timezones) ; i++) {
	for (int j = 0 ; j < timezones[i].eras_count ; j++) {
	    for (int k = 0 ; k < timezones[i].eras[j].rules_count ; k++) {
		const char *rule = timezones[i].eras[j].rules[k].abbreviation;
		const char *era  = timezones[i].eras[j].abbreviation_std;

		if (strchr(era, '%') != NULL) {
		    char fullabbr[16];
		    snprintf(fullabbr, sizeof(fullabbr), era, rule);
		    printf("%s\n", fullabbr);
		}
	    }
	}
    }
}
